import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new Component(
		'section',
		null,
		'Ici vous pourrez ajouter une pizza'
	);

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate('/'); // affiche la liste des pizzas

document.querySelector('.pizzaFormLink');
document.querySelector('.pizzaList h4:first-of-type').textContent;

document.querySelectorAll('.mainMenu  a');
document.querySelectorAll('.pizzaList  li');
document.querySelectorAll('.pizzaList h4')[1].textContent;

let logo = document.querySelector('.logo');
logo.innerHTML += "<small>les pizzas c'est la vie</small>";

document.querySelectorAll('footer a')[1].getAttribute('href');
let menu = document.querySelectorAll('.mainMenu li')[0];
menu.innerHTML =
	'<li><a href="/" class="pizzaListLink active">La carte</a><li>';

let container = document.querySelector('.newsContainer');
container.setAttribute('style', 'display: flex');

let close = document.querySelector('.closeButton');
close.addEventListener('click', event => {
	container.setAttribute('style', 'display: none');
});

Router.menuElement = document.querySelector('.mainMenu');
